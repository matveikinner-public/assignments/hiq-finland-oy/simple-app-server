# Simple App Server

## Installation

### Prerequisites

**Important!** You will require both [Simple App UI](https://gitlab.com/matveikinner-public/assignments/hiq-finland-oy/simple-app-ui) and [Simple App Server](https://gitlab.com/matveikinner-public/assignments/hiq-finland-oy/simple-app-server) repositories from GitLab in the same parent directory to be able to launch / orchestrate all Docker containers required to run the application. You will also require Docker as the application uses PostgreSQL official [Dockerhub](https://hub.docker.com/_/postgres) container to "mock" database

- Simple App UI launches on localhost port 8080
- Simple App Server launches on localhost port 80

### Scripts

#### Makefile

To launch application containers with tiny shell CLI you can use Makefile `make` commands (if not on Unix system see [StackOverflow](https://stackoverflow.com/a/32127632/12660598))

To launch / orchestrate all Docker containers run

    make docker

Then in the CLI select

1. **Option 3** to "Compose"
2. **Option 1** to "Up"

#### Docker

To launch / orchestrate all Docker containers run

    docker-compose -f docker-compose.development.yml up

#### Yarn

_In case you want to ex. shut down UI or Server and run them locally yourself. However, the server database connection will still require PostgreSQL container running_

To install dependencies run

    yarn install

To run development version of the application with Webpack run

    yarn start:dev

To build production version of the application with Webpack run

    yarn build:prod

To launch production version of the application consider installing ex. http-server though Docker is de-factor way to run the application

## Solution

### Features

- REST API with endpoints to GET (retrieve single or all short urls) and POST (to create a short url)
- PostgreSQL as a database (as Docker container) with TypeORM
- Full request / query param validation
- Top-notch (as it comes to performance) custom logger with Pino
- As per assignment data / short urls have expiration / validity for 7 days
- Scheduler using Cron to automatically delete all expired short urls from database every 5 minutes
- Postman collection and environment files
- Consideration for API versioning in service methods (handlers) with query param in URI (excluding as per assignment base path to GET by short url the original url data)
- OpenAPI 3.0 specs with Swagger
- Various scripts and deployment options to make it easy to run the application (though relies solely on Docker)
- Partial unit tests

### Learnings

- Extracting host data / params to parse domain as localhost in order to create short url links

### Caveats

- No cache solution such as ex. Redis
- Partially poor unit tests
- No deployment to Azure (can be done as GitLab CI / CD is nice, though PostgreSQL costs few bucks to provision so Docker will do)
- Unlike in an assignment it was chosen to return full / proper JSON response from short url GET request as it makes Imo no sense to return only the original url as a string in REST API response. The other option would have been to automatically redirect the short url to the original url which would have been the de-facto way to implement this

### Bonus Task (as per assignment)

> Bonus task: please compose a list of ToDo actions needed to bring the code to production stage.

- Proper environments in ex. Azure / AWS with development, staging and production environment (including things such as VPN, one or multiple load balancer, VMs etc.). This should include Kubernetes Cluster or Docker Swarm to orchestrate and manage containers and in most cases system wide log / monitor solutions such as ex. [ELK -stack](https://www.elastic.co/what-is/elk-stack)
- Proper domain to be able to provide short urls beyond localhost / IP -addresses
- Deployment CI / CD pipelines from version control all the way to the ex. cloud with ex. GitLab CI, runners and webhooks

## Documentation

### Conventions

#### Commits

The [Commitlint](https://commitlint.js.org/#/) enforces best practices together with [Commitlint Jira rules](https://github.com/Gherciu/commitlint-jira). The commit message issue status must comply with [default Jira statuses](https://confluence.atlassian.com/adminjiracloud/issue-statuses-priorities-and-resolutions-973500867.html) which follow task ID (from Jira board or as running repository prefix and enumeration), task description (as abbreviation), and commit message which must be in present tense. These must all be in capital letters.

Format

    git commit -m "[<ISSUE-STATUS>]<TASK-ID>: [<TASK-DESCRIPTION>] <message>"

As an example

    git commit -m "[CLOSED]TASK-1: [CONF] Create .gitlab-ci.yml production branch CI / CD pipeline

##### Task descriptions

- [CONF] Informs that the ticket task description concerns configuration
- [FEAT] Informs that the ticket task description concerns feature
- [UPDT] Informs that the ticket task description concerns update to an existing feature / capability
- [FIX] Informs that the ticket task description concerns fix to a particular known issue / bug

_The option to include custom commit rules to include all possible scenarios is up to later consideration_

#### Branching Strategy

##### Overview

There are three main branches — development, staging, and production. These branches contain CI / CD pipeline which builds to corresponding domain / FQDN ex. https://\<branch>.\<domain>.\<domain-extension>

```mermaid

graph LR

T(Ticket) --> D(Development)

T(Ticket) --> F(Feature)

F --> D

D --> M(Master)

M --> S(Staging)

S --> P(Production)

```

##### Ticket

The ticket branch can branch out from either development branch or feature branch. Once complete the ticket branch merges always back to its parent branch. The only exceptions are unconventional situations where there is requirement to ex. cherry pick a particular commit higher in the tree to patch issues which require immediate attention. The ticket branch name derives from Jira board in use.

##### Feature

The feature branch can branch out only from the development branch. Once complete the feature branch merges always back to its parent branch. The feature branch name has "feature-" prefix which follows brief feature description ex. feature-password-reset.

##### Development

The development branch contains latest code.

##### Master

The master branch must contain at all times ready code ready for release to staging.

##### Staging

The staging branch must contain at all time ready code ready for release to production. Releases to this branch require appropriate version tag as a release candidate postfix and message.

Format

    git tag -a <semver>_rc<number> -m "<message>"

As an example

    git tag -a 1.0.0_rc1 -m "Release 1.0.0~rc1 to staging"

##### Production

The production branch contains latest official release. Releases to this branch require appropriate version tag and message.

Format

    git tag -a <semver> -m "<message>"

As an example

    git tag -a 1.0.0 -m "Release 1.0.0 to production"

#### Versions

The [Semantic Versioning](https://semver.org/) applies.
