import { NestFactory } from "@nestjs/core";
import { RequestMethod, ValidationPipe, ValidationPipeOptions, VersioningType } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { DocumentBuilder, SwaggerModule } from "@nestjs/swagger";
import { LoggerService } from "@shared/logger/logger.service";
import { AppModule } from "./app.module";
import ConfigEnum, { SharedConfigOptions } from "./configs";
import TypeORMExceptionFilter from "@shared/filters/typeorm-exception.filter";

const bootstrap = async () => {
  const app = await NestFactory.create(AppModule, {
    logger: new LoggerService(),
  });

  const configService = app.get(ConfigService);

  const sharedConfig = configService.get<SharedConfigOptions>(ConfigEnum.SHARED);
  const validationConfig = configService.get<ValidationPipeOptions>(ConfigEnum.VALIDATION);

  app.setGlobalPrefix("api", { exclude: [{ path: "/:url", method: RequestMethod.GET }] });
  app.enableCors();
  app.enableVersioning({
    type: VersioningType.URI,
  });
  app.useGlobalPipes(new ValidationPipe(validationConfig));
  app.useGlobalFilters(new TypeORMExceptionFilter());

  const swaggerOptions = new DocumentBuilder()
    .setTitle(sharedConfig.name)
    .setDescription("API Documentation for Simple App Server")
    .setVersion(sharedConfig.version)
    .build();

  const swaggerDoc = SwaggerModule.createDocument(app, swaggerOptions);

  SwaggerModule.setup("/api/docs", app, swaggerDoc);

  await app.listen(sharedConfig.port);
};

void bootstrap();
