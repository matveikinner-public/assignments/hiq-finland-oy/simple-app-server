/* eslint-disable @typescript-eslint/no-unsafe-argument, @typescript-eslint/no-unsafe-call, @typescript-eslint/no-unsafe-member-access */
import { Injectable, Inject, Optional } from "@nestjs/common";
import pino, { Logger } from "pino";
import { ILoggerService } from "./interfaces/logger.interface";
import { PINO_LOGGER_OPTIONS } from "./logger.constants";
import Params from "./interfaces/params.interface";

@Injectable()
export class LoggerService implements ILoggerService {
  private readonly logger: Logger;

  constructor(
    @Optional() private context?: string,
    @Inject(PINO_LOGGER_OPTIONS) private readonly params?: Params | undefined
  ) {
    if (this.params) {
      this.logger = pino(this.params.options);
    } else this.logger = pino();
  }

  verbose(message: string, context?: string, data?: Record<string, unknown>, ...args: any[]): void {
    this.logger.trace({ context: context || this.context, ...data }, message, ...args);
  }

  debug(message: string, context?: string, data?: Record<string, unknown>, ...args: any[]): void {
    this.logger.debug({ context: context || this.context, ...data }, message, ...args);
  }

  log(message: string, context?: string, data?: Record<string, unknown>, ...args: any[]): void {
    this.logger.info({ context: context || this.context, ...data }, message, ...args);
  }

  warn(message: string, context?: string, data?: Record<string, unknown>, ...args: any[]): void {
    this.logger.warn({ context: context || this.context, ...data }, message, ...args);
  }

  error(message: string, trace?: string, context?: string, ...args: any[]): void {
    if (trace) this.logger.error({ context: context || this.context, trace }, message, ...args);
    this.logger.error({ context: context || this.context }, message, ...args);
  }

  fatal(message: string, ...args: any[]): void {
    this.logger.fatal(message, ...args);
  }

  middleware(message: string, context?: string, data?: any, ...args: any[]): void {
    this.logger.info(data, message, { context: context || this.context }, ...args);
  }

  setContext(context: string): void {
    this.context = context;
  }
}
