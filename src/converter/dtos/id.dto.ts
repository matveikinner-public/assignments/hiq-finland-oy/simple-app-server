import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, Length } from "class-validator";

export default class IdDto {
  @ApiProperty({
    type: String,
    required: true,
    description: "The ID of the tiny URL",
    example: "http://localhost:3000/3f411d128914adfc",
  })
  @IsNotEmpty()
  @IsString()
  @Length(16, 16)
  readonly url: string;
}
