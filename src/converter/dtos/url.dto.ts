import { ApiProperty } from "@nestjs/swagger";
import { IsNotEmpty, IsString, IsUrl } from "class-validator";

export default class UrlDto {
  @ApiProperty({
    type: String,
    required: true,
    description: "The original URL to shorten",
    example: "https://hiqfinland.fi/avoimet-tyopaikat/",
  })
  @IsNotEmpty()
  @IsString()
  @IsUrl({
    protocols: ["http", "https"],
    require_protocol: true,
    require_host: true,
    require_valid_protocol: true,
  })
  readonly url: string;
}
