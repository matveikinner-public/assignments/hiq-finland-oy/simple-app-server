import { Injectable } from "@nestjs/common";
import { InjectRepository } from "@nestjs/typeorm";
import { randomBytes } from "crypto";
import { addDays } from "date-fns";
import { MoreThan, Repository } from "typeorm";
import { UrlEntity } from "./entities/url.entity";

@Injectable()
export class ConverterService {
  constructor(
    @InjectRepository(UrlEntity)
    private urlsRepository: Repository<UrlEntity>
  ) {}

  encode = async (url: string, secure: boolean, host: string) => {
    const newUrlEntity = this.urlsRepository.create({
      originalUrl: url,
      tinyUrl: `${secure ? "https" : "http"}://${host}/${randomBytes(8).toString("hex")}`,
      validUntil: addDays(new Date(), 7),
    });

    return await this.urlsRepository.save(newUrlEntity);
  };

  decode = async (url: string, secure: boolean, host: string) =>
    await this.urlsRepository.findOneOrFail({
      where: {
        tinyUrl: `${secure ? "https" : "http"}://${host}/${url}`,
        validUntil: MoreThan(new Date()),
      },
    });

  getUrls = async () => await this.urlsRepository.find();
}
