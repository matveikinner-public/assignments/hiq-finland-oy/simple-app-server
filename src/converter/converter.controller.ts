import { Controller, Get, Param, Post, Req, Version } from "@nestjs/common";
import { Request } from "express";
import { ConverterService } from "./converter.service";
import IdDto from "./dtos/id.dto";
import UrlDto from "./dtos/url.dto";

@Controller("")
export class ConverterController {
  constructor(private readonly converterService: ConverterService) {}

  @Version("1")
  @Post("converter/:url")
  async encode(@Param() params: UrlDto, @Req() req: Request) {
    return await this.converterService.encode(params.url, req.secure, req.headers.host);
  }

  @Version("1")
  @Get("converter")
  async getUrls() {
    return await this.converterService.getUrls();
  }

  @Get(":url")
  async decode(@Param() params: IdDto, @Req() req: Request) {
    return await this.converterService.decode(params.url, req.secure, req.headers.host);
  }
}
