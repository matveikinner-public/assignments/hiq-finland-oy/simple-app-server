/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Test, TestingModule } from "@nestjs/testing";
import { ConverterController } from "./converter.controller";
import { ConverterService } from "./converter.service";
import { createRequest } from "node-mocks-http";

describe("ConverterController", () => {
  let controller: ConverterController;
  let service: ConverterService;

  const req = createRequest();
  const mockUrlEntity = {
    id: expect.any(String),
    originalUrl: "https://hiqfinland.fi/avoimet-tyopaikat/",
    tinyUrl: expect.any(String),
    validUntil: expect.any(Date),
    createdAt: expect.any(Date),
    updatedAt: expect.any(Date),
  };

  const mockConverterService = {
    encode: jest.fn((url: string, secure: boolean, host: string) => ({
      id: expect.any(String),
      originalUrl: url,
      tinyUrl: expect.any(String),
      validUntil: expect.any(Date),
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
    })),
    decode: jest.fn((url: string, secure: boolean, host: string) => ({
      id: expect.any(String),
      originalUrl: expect.any(String),
      tinyUrl: url,
      validUntil: expect.any(Date),
      createdAt: expect.any(Date),
      updatedAt: expect.any(Date),
    })),
  };

  beforeEach(async () => {
    const moduleRef: TestingModule = await Test.createTestingModule({
      providers: [ConverterService],
      controllers: [ConverterController],
    })
      .overrideProvider(ConverterService)
      .useValue(mockConverterService)
      .compile();

    controller = moduleRef.get<ConverterController>(ConverterController);
    service = moduleRef.get<ConverterService>(ConverterService);
  });

  it("should be defined", () => {
    expect(controller).toBeDefined();
  });

  it("should encode an url", async () => {
    expect(await controller.encode({ url: "https://hiqfinland.fi/avoimet-tyopaikat/" }, req)).toEqual(mockUrlEntity);
    expect(service.encode).toHaveBeenCalled();
  });

  it("should decode an url", async () => {
    expect(await controller.decode({ url: "http://localhost:3000/205402804a49f511" }, req)).toEqual(mockUrlEntity);
    expect(service.decode).toHaveBeenCalled();
  });
});
