import { Injectable } from "@nestjs/common";
import { Cron, CronExpression } from "@nestjs/schedule";
import { InjectRepository } from "@nestjs/typeorm";
import { LoggerService } from "@shared/logger/logger.service";
import { LessThan, Repository } from "typeorm";
import { UrlEntity } from "./entities/url.entity";

@Injectable()
export class ConverterScheduler {
  constructor(
    private readonly logger: LoggerService,
    @InjectRepository(UrlEntity)
    private readonly urlsRepository: Repository<UrlEntity>
  ) {
    this.logger.setContext(ConverterScheduler.name);
  }

  @Cron(CronExpression.EVERY_5_MINUTES)
  async cleanExpiredUrls() {
    try {
      this.logger.log("Attempting to delete expired urls");
      const expiredUrls = await this.urlsRepository.find({
        where: {
          validUntil: LessThan(new Date()),
        },
      });

      this.logger.log(`Found ${expiredUrls.length} expired urls`);
      await this.urlsRepository.remove(expiredUrls);

      this.logger.log("Success while attempting to delete expired urls");
    } catch (err) {
      this.logger.error("Error while attempting to delete expired urls");
      throw err;
    }
  }
}
