import { Module } from "@nestjs/common";
import { ConverterService } from "./converter.service";
import { ConverterController } from "./converter.controller";
import { UrlEntity } from "./entities/url.entity";
import { TypeOrmModule } from "@nestjs/typeorm";
import { ConverterScheduler } from "./converter.scheduler";

@Module({
  imports: [TypeOrmModule.forFeature([UrlEntity])],
  providers: [ConverterService, ConverterScheduler],
  controllers: [ConverterController],
})
export class ConverterModule {}
