export default interface IUrl {
  id: string;
  originalUrl: string;
  tinyUrl: string;
  validUntil: Date;
  createdAt: Date;
  updatedAt: Date;
}
