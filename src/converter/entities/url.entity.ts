import { BaseEntity, PrimaryGeneratedColumn, Column, Entity, CreateDateColumn, UpdateDateColumn } from "typeorm";
import IUrl from "../interfaces/url.interface";

@Entity()
export class UrlEntity extends BaseEntity implements IUrl {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  originalUrl: string;

  @Column({ unique: true })
  tinyUrl: string;

  @Column()
  validUntil: Date;

  @CreateDateColumn({ type: "timestamptz", default: () => "now()" })
  public createdAt: Date;

  @UpdateDateColumn({ type: "timestamptz", default: () => "now()", onUpdate: "now()" })
  public updatedAt: Date;
}
