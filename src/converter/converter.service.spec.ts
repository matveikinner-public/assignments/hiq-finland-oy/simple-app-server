/* eslint-disable @typescript-eslint/no-unsafe-assignment */
import { Test, TestingModule } from "@nestjs/testing";
import { getRepositoryToken } from "@nestjs/typeorm";
import { createRequest } from "node-mocks-http";
import { DeepPartial } from "typeorm";
import { ConverterController } from "./converter.controller";
import { ConverterService } from "./converter.service";
import { UrlEntity } from "./entities/url.entity";

describe("ConverterService", () => {
  let service: ConverterService;

  const req = createRequest();

  const mockUrlEntity = {
    id: expect.any(String),
    originalUrl: "https://hiqfinland.fi/avoimet-tyopaikat/",
    tinyUrl: expect.any(String),
    validUntil: expect.any(Date),
    createdAt: expect.any(Date),
    updatedAt: expect.any(Date),
  };

  const mockUrlsRepository = {
    create: jest.fn().mockImplementationOnce((entityLike: DeepPartial<UrlEntity>) => entityLike as UrlEntity),
    save: jest
      .fn()
      .mockImplementation((urlEntity) =>
        Promise.resolve({ id: "", createdAt: new Date(), updatedAt: new Date(), ...urlEntity })
      ),
    find: jest.fn().mockImplementation(() => Promise.resolve([] as UrlEntity[])),
    findOneOrFail: jest.fn().mockImplementation(() => Promise.resolve(mockUrlEntity as UrlEntity)),
  };

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        ConverterService,
        {
          provide: getRepositoryToken(UrlEntity),
          useValue: mockUrlsRepository,
        },
      ],
      controllers: [ConverterController],
    }).compile();

    service = module.get<ConverterService>(ConverterService);
  });

  it("should be defined", () => {
    expect(service).toBeDefined();
  });

  it("should return encoded url", async () => {
    expect(await service.encode("https://hiqfinland.fi/avoimet-tyopaikat/", false, "http")).toEqual(mockUrlEntity);
  });

  it("should return decoded url", async () => {
    expect(await service.decode("http://localhost:3000/205402804a49f511", req.secure, req.headers.host)).toEqual(
      mockUrlEntity
    );
    expect(mockUrlsRepository.findOneOrFail).toHaveBeenCalled();
  });
});
