import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { APP_INTERCEPTOR } from "@nestjs/core";
import { ScheduleModule } from "@nestjs/schedule";
import { TypeOrmModule } from "@nestjs/typeorm";
import { TransformInterceptor } from "@shared/interceptors/transform.interceptor";
import { LoggerModule } from "@shared/logger/logger.module";
import ConfigEnum, { sharedConfig, databaseConfig, loggerConfig } from "./configs";
import { ConverterModule } from "./converter/converter.module";

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [sharedConfig, loggerConfig, databaseConfig],
      isGlobal: true,
    }),
    LoggerModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: async (configService: ConfigService) => await configService.get(ConfigEnum.LOGGER),
      inject: [ConfigService],
    }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      useFactory: (configService: ConfigService) => configService.get(ConfigEnum.DATABASE),
      inject: [ConfigService],
    }),
    ScheduleModule.forRoot(),
    ConverterModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: TransformInterceptor,
    },
  ],
})
export class AppModule {}
