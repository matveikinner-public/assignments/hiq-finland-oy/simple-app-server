import { registerAs } from "@nestjs/config";
import ConfigEnum from "../enums/config.enum";

export interface SharedConfigOptions {
  port: number;
  name: string;
  version: string;
}

export default registerAs(
  ConfigEnum.SHARED,
  (): SharedConfigOptions => ({
    port: parseInt(process.env.PORT) || 80,
    name: process.env.NODE_ENV ? `Simple App API - ${process.env.NODE_ENV.toUpperCase()}` : "Simple App API",
    version: process.env.VERSION || "0.0.1",
  })
);
